/**
 * serach
 */
public class serach {

    static boolean searach(int arr[], int n, int sum) {

        if (sum == 0)
            return true;
        if (n == 0)
            return false;

        if (arr[n - 1] > sum)
            return searach(arr, n - 1, sum);

        return searach(arr, n - 1, sum) ||
                searach(arr, n - 1, sum - arr[n - 1]);
    }

    public static void main(String args[]) {
        int set[] = { 3, 34, 4, 12, 5, 2 };
        int sum = 9;
        int n = set.length;
        if (searach(set, n, sum) == true)
            System.out.println("Found a subset"
                    + " with given sum");
        else
            System.out.println("Not found a subset with"
                    + " given sum");
    }
}
